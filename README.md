# Tensorflow devcontainer 



## In VSCode
You need docker installed and usable as a normal user (e.g. `usermod -aG docker $(whoami)` on Linux, default on Windows/Mac).  See the Docker section below if unfamiliar with this technology.


Install the remote container extension in VSCode.  In the left pane, click the `extensions` button (the icon looks like Tetris (tm)).  Then in the search bar that appears type `ms-vscode-remote.remote-containers`.  Select install.

Then open the folder containing the `.devcontainer` folder in VSCode.  It's the same folder in which  this `README.md` is located; alternatively it's the folder where you cloned the repo from gitlab.

When you open this folder in VSCode (*), the editor will detect the devcontainer environment and a pop-up message will appear in the bottom right of the editor.  

    Folder contains a Dev Container configuration file. Reopen folder to develop in a container ([learn more](https://aka.ms/vscode-remote/docker)).

Click the `reopen in container` button to launch VSCode in the docker container.  The first run through, the images and tools will be pulled in and might take a few minutes.  You can view progress by clicking the `Show log` button.

Then open the `test.py` or the `test_nb.ipynb` file.  The `test.py` can be loaded into the integrated terminal in VSCode (the terminal is in the docker container, so nothing to do).  The notebook file runs directly in VSCode with a small caveat that not all jupyter notebook extensions are available.  If these notebook extensions are desired, see the Direct section below.

## Direct 

We need to build and run a different docker container than the default devcontainer image in order to make sure jupyter and its dependencies are loaded.  To do this, open a terminal and do

    cd .devcontainer
    docker build -f tf_nb.dockerfile -t tf_nb .


The above tells docker to build the description in `tf_nb.dockerfile` (the `-f` flag) into a container named `tf_nb` (the `-t` flag).  The trailing `.` is not optional and says to do this in the current directory.  

Then we need to run the container.  If you inspect the dockerfile, you'll see we run the jupyter notebook on port `9898` in the container, so when we run it, we need to map our port `9898` to port `9898` in the container.  This is acheived with 

    docker run -p 9898:9898 tf_nb

Then open a web-browser and navigate to `127.0.0.1:9898` or `http://127.0.0.1:9898`.  Alternatively click the links as indicated in the terminal.

We can also set the image to clean up when we leave.  To do this run with th `--rm` flag.

    docker run --rm -p 9898:9898 tf_nb

Finally, note that there are no local files available in the container.  Allow this by passing a volume into the container.  To pass the directory you're currently in to the container do:

    docker run --rm -p 9898:9898 -v $(pwd):/home/tfUser/workspace tf_nb

This maps the current directory into the directory /home/tfUser/workspace in the container.  Open a web-browser and go to `127.0.0.1:9898` and you'll see a directory.  In this directory, navigate to `/home/tfUser/workspace`.  You'll see all the files in the current directory.  In particular, if the current directory is the directory in which this `README.md` is contained, then you'll see the `test_nb.ipynb` in the folder.  Open this file to begin the notebook session.



## Docker
This repo hosts a `.devcontainer` environment to quickly get up and running with tensorflow.  This requires the Docker (tm) system to be running on the computer.  If you haven't used Docker before or in a while, there are two versions, Docker Engine available for Linux-based systems and Docker Desktop available for all systems.  However, note, that while Docker Engine has an open source license, Docker Desktop does not.

  - Install Docker Engine on a Linux-based system.  Go [Docker Engine Install Official Docker Docs](https://docs.docker.com/engine/install/).  Then in the menu on the left click `Installation per distro` and select the distro of choice and follow instructions.
  - Install Docker Desktop on Windows.  The best experience is to install a WSL2 Linux distro first [WSL install Official Microsoft Docs](https://docs.microsoft.com/en-us/windows/wsl/install).  Then install the Docker Desktop application [Docker Desktop Windows Official Docker Docs](https://docs.docker.com/desktop/windows/install/) and choose to use the WSL2 backend.  This gives performance close to running Docker on Linux-based system.
  - Install Docker Desktop on Mac.  See [Install Docker Desktop Mac Official Docker Docs](https://docs.docker.com/desktop/mac/install/).  
  - It's possible to install Docker Engine directly into WSL2 on Windows without installing Docker Desktop.  Search around for "install docker directly in wsl2."
  - It's also possible to run a Linux VM (Virtualbox, Hyper-V, VMWare, Parallels) and install Docker Engine in the VM.  If setup with GPU, VT-x, VT-d, and IOMMU passthrough, there shouldn't be much performance hit.
