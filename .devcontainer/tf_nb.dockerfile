# Use most recent Ubuntu.  
FROM ubuntu:latest


#*********************************
# Set up build time stuff
#*********************************
# These args can be overridden at build time if required/desired
ARG uid=1000
ARG gid=1000
# Make apt not ask too many questions
ARG DEBIAN_FRONTEND=noninteractive

#*********************************
# Set up environment stuff
#*********************************
ENV LANGUAGE=C.UTF-8
ENV LC_ALL=C.UTF-8
ENV USER_NAME=tfUser
ENV PATH=${PATH}:/home/${USER_NAME}/.local/bin
#*********************************
# Initialize system
#*********************************
RUN apt-get update 
RUN apt-get dist-upgrade -y

#*********************************
# Fetch supplies
# Add additional Ubuntu packages here
#*********************************
RUN apt-get install apt-utils build-essential git wget libgmp-dev coreutils cmake ca-certificates -y
RUN apt-get install python3 -y
RUN apt-get install python3-pip -y


#*********************************
# Cleanup apt stuff
#*********************************
USER root
RUN apt-get autoremove -y
RUN apt-get clean -y
RUN rm -rf /var/lib/apt/lists/*



#*********************************
# Initialize and switch users
# Note if you need to do other stuff as root in the dockerfile, do a `User root` then afterwards do `User ${USER_NAME}`
#*********************************
RUN groupadd -r --gid ${gid} ${USER_NAME}
RUN useradd -r --create-home --shell /bin/bash --uid ${uid} --gid ${gid} ${USER_NAME}
RUN chown ${USER_NAME} /home/${USER_NAME}
USER ${USER_NAME}
 
#*********************************
# Install python libraries
#*********************************
RUN python3 -m pip 
RUN python3 -m pip install pylint
RUN python3 -m pip install tensorflow
RUN python3 -m pip install ipykernel
RUN python3 -m pip install jupyter matplotlib

#*********************************
# Turn on Jupyter notebook
#*********************************
EXPOSE 9898 
CMD ["jupyter","notebook","--port=9898","--no-browser","--ip=0.0.0.0"]